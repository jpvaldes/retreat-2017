# Retreat 2017

This repository contains some notebooks used to classify perceptual stimuli
from MEG data. The data was already pre-processed beforehand.

## Group Members

Mario B López Pérez, Jonathan P Shine, José P Valdés Herrera. 

## References

The classification task follows closely Kurth-Nelson's publications:

- Temporal structure in associative retrieval, Zeb Kurth-Nelson, Gareth Barnes,
  Dino Sejdinovic, Ray Dolan, Peter Dayan, eLife, 2015.
- Fast Sequences of Non-spatial State Representations in Humans, Zeb
  Kurth-Nelson, Marcos Economides, Raymond J. Dolan, Peter Dayan, Neuron, 2017.
